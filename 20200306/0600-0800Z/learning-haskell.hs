{-# OPTIONS_GHC -Wall #-}

x :: Integer
x = 99

f :: Integer -> Integer
f a = a + 10

g :: Integer -> Integer -> Integer
g a b = (a + b) * 2

h :: (Integer -> a) -> a
h v = v 99

i :: (Integer -> a) -> a
i = \v -> v 99

j :: anything -> someotheranything -> anything
j d e = d

k :: w -> w -> w
k = \q -> \r -> r

len :: [a] -> Integer
len = const 99

l :: Integer -> q -> Integer
l = \n -> \q -> n + 99

m :: Bool -> Bool
m = \b -> b

data Shape =
  Circle Integer
  | Rectangle Integer Integer
  | Triangle Integer Integer Integer
  deriving (Eq, Show)

perimeter :: Shape -> Integer
perimeter = \s -> case s of
  Circle r -> r * 3 * 2
  Rectangle w h -> (w + h) * 2
  Triangle a b c -> a + b + c

perimeteragain :: Shape -> Integer
perimeteragain (Circle r) = r * 3 * 2
perimeteragain (Rectangle w h) = (w + h) * 2
-- perimeteragain (Triangle a b c) = 99
-- perimeteragain (Triangle _ _ _) = 99
perimeteragain _ = 99

data UpToThree a =
  Zero | One a | Two a a | Three a a a
  deriving (Eq, Show)

mapThree :: (a -> b) -> UpToThree a -> UpToThree b
mapThree = \f -> \t -> case t of
  Zero -> Zero
  One a -> One (f a)
  Two a1 a2 -> Two (f a1) (f a2)
  Three a1 a2 a3 -> Three (f a1) (f a2) (f a3)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

{-
getHead :: List a -> a
getHead (Cons h _) = h
getHead Nil = ?
-}

headOr :: a -> List a -> a
headOr = \a -> \list -> case list of
  Nil -> a
  Cons h _ -> h

mapList :: (a -> b) -> List a -> List b



