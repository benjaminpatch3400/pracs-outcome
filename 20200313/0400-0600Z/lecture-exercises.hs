import Prelude hiding (sum, (++))

data Optional a = Empty | Full a

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional func optl =
  case optl of
    Empty -> error "todo"
    Full x -> error "todo"

bindOptional :: (a -> Optional b) -> Optional a -> Optional b
bindOptional = error "todo"

data List a = Nil | Cons a (List a) deriving (Eq, Show)

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil = b
foldRight f b (Cons h t) = f h (foldRight f b t)

-- adds up the numbers in a list
sum :: List Integer -> Integer
sum = foldRight (+) 0

-- appends two lists
(++) :: List x -> List x -> List x
(++) x y = foldRight Cons y x
{-
(++) Nil y = y
-- h :: x
-- t :: List x
-- y :: List x
-- _ :: List x
(++) (Cons h t) y = Cons h (t ++ y)
-}
-- flattens a list of lists
flatten :: List (List x) -> List x
-- h :: List x
            -- t :: List (List x)
-- flatten t :: List x
-- _ :: List x
flatten Nil = Nil
flatten (Cons h t) = h ++ flatten t
-- flatten = \list -> foldRight (++) Nil list
{-
eta-reduce
\x -> f x
f
-}
-- reverses a list
reverse :: List x -> List x
reverse = reverse0 Nil

reverse0 :: List x -> List x -> List x
reverse0 acc Nil = acc
reverse0 acc (Cons h t) = reverse0 (Cons h acc) t

data Or a b = IsA a | IsB b

modifyA :: (a -> a) -> Or a b -> Or a b
modifyA = error "todo"

getB :: Or a b -> Optional b
getB = error "todo"

data Tree a = Tree a (List (Tree a))

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree = error "todo"

applyOptional2 ::
  (a -> b -> c) -> (Optional a -> Optional b -> Optional c)
applyOptional2 _ Empty _ = Empty
applyOptional2 _ _ Empty = Empty
applyOptional2 f (Full a) (Full b) = Full (f a b)

traverseListOptional ::
  (a -> Optional b) -> List a -> Optional (List b)
traverseListOptional _ Nil = Full Nil
traverseListOptional f (Cons h t) = 
  applyOptional2 Cons (f h) (traverseListOptional f t)
  -- h :: a
  -- t :: List a
  -- f :: a -> Optional b

  -- f h ::                     Optional b
  -- traverseListOptional f t = Optional (List b)
  -- _ ::                       Optional (List b)


traverseTreeOptional ::
  (a -> Optional b) -> Tree a -> Optional (Tree b)
traverseTreeOptional f (Tree r c) =
  applyOptional2
    Tree
    (f r)
    (traverseListOptional (\x -> traverseTreeOptional f x) c)

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

-- the error message
parseError :: ParseResult x -> Optional String
parseError = error "todo"
-- return a parser that succeeds, puts the input to the output
neutralParser :: x -> Parser x
neutralParser = error "todo"
-- a parser that consumes one character of input (if it can)
character :: Parser Char
character = error "todo"

data Ordering' = LessThan | EqualTo | GreaterThan
class Eq x => Order x where
  compare :: x -> x -> Ordering'

data Vector6 a = Vector6 a a a a a a

maximumV6 :: Order a => Vector6 a -> a
maximumV6 = error "todo"
reverseV6 :: Order a => Vector6 a -> Vector6 a
reverseV6 = error "todo"

mapList ::     (a -> b) -> List     a -> List     b
mapList = error "alreadyDone"

-- mapOptional :: (a -> b) -> Optional a -> Optional b
-- mapOptional = error "already done"

mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapParser = error "already done"

mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
mapVector6 = error "already done"

flipList ::
  List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list

flipOptional ::
  Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt

flipParser ::
  Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr

flipVector6 ::
  Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6

class ThingThatMaps k where
  mappy :: (a -> b) -> k a -> k b

instance ThingThatMaps List where
  mappy = mapList

instance ThingThatMaps Optional where
  mappy = mapOptional

flipAnything ::
  ThingThatMaps k => k (a -> b) -> a -> k b
flipAnything x a =
  mappy (\func -> func a) x

instance ThingThatMaps ((->) anything) where
  






