data Optional a = Empty | Full a

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional func optl =
  case optl of
    Empty -> error "todo"
    Full x -> error "todo"

bindOptional :: (a -> Optional b) -> Optional a -> Optional b
bindOptional = error "todo"

data List a = Nil | Cons a (List a)

-- adds up the numbers in a list
sum :: List Integer -> Integer
sum = error "todo"
-- appends two lists
(++) :: List x -> List x -> List x
(++) = error "todo"
-- flattens a list of lists
flatten :: List (List x) -> List x
flatten = error "todo"
-- reverses a list
reverse :: List x -> List x
reverse = error "todo"

data Or a b = IsA a | IsB b

modifyA :: (a -> a) -> Or a b -> Or a b
modifyA = error "todo"

getB :: Or a b -> Optional b
getB = error "todo"

data Tree a = Tree a (List (Tree a))

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree = error "todo"

traverseTreeOptional ::
  (a -> Optional b) -> Tree a -> Optional (Tree b)
traverseTreeOptional = error "todo"

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

-- the error message
parseError :: ParseResult x -> Optional String
parseError = error "todo"
-- return a parser that succeeds, puts the input to the output
neutralParser :: x -> Parser x
neutralParser = error "todo"
-- a parser that consumes one character of input (if it can)
character :: Parser Char
character = error "todo"

data Ordering' = LessThan | EqualTo | GreaterThan
class Eq x => Order x where
  compare :: x -> x -> Ordering'

data Vector6 a = Vector6 a a a a a a

maximumV6 :: Order a => Vector6 a -> a
maximumV6 = error "todo"
reverseV6 :: Order a => Vector6 a -> Vector6 a
reverseV6 = error "todo"

mapList ::     (a -> b) -> List     a -> List     b
mapList = error "alreadyDone"

-- mapOptional :: (a -> b) -> Optional a -> Optional b
-- mapOptional = error "already done"

mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapParser = error "already done"

mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
mapVector6 = error "already done"

