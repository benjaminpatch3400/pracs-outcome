-- | This avoids importing Prelude.
-- Below I had to explicitly import several modules
-- that are imported by default in Prelude.
{-# LANGUAGE NoImplicitPrelude #-}

-- | This lets you put type signatures in instances
-- e.g. for fmap
{-# LANGUAGE InstanceSigs #-}

-- | This enables most compiler warnings.
-- You can also `:set -Wall` in GHCi.
-- I recommend enabling warnings.
--
{-# OPTIONS_GHC -Wall #-}

module Prac3 where

import Data.String
import Data.Bool
import Data.Eq
import Data.Ord
import Data.Char (Char)
import Text.Show

import Prelude (error, (+), Int)


{- DATA TYPES AND CLASSES -}

data Optional a = Empty | Full a
  deriving (Show)

data List a = Nil | Cons a (List a)
  deriving (Show)

data Pair a b = Pair a b

data Or a b = IsA a | IsB b

data NonEmptyList a = NonEmptyList a (List a)

data Tree a = Tree a (List (Tree a))

data Vector6 a = Vector6 a a a a a a

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

class Functor f where
  fmap :: (a -> b) -> f a -> f b


{- EXERCISE 1: Functor instances #-}

instance Functor Optional where
  fmap :: (a -> b) -> Optional a -> Optional b
  fmap f Empty = Empty
  fmap f (Full a) = Full (f a)

instance Functor Vector6 where
  fmap f (Vector6 a1 a2 a3 a4 a5 a6) =
    Vector6 (f a1) (f a2) (f a3) (f a4) (f a5) (f a6) 

instance Functor List where
  fmap f (Cons a b) = Cons (f a) (fmap f b)
  fmap _ Nil = Nil

instance Functor NonEmptyList where
  -- fmap :: (a -> b) -> List a -> List b
  fmap f (NonEmptyList a as) =
    NonEmptyList (f a) (fmap f as)

-- data Tree a = Tree a (List (Tree a))

instance Functor Tree where
  fmap f (Tree a (Cons b c)) = Tree (f a) (mapListOfTrees f (Cons b c))
  fmap f (Tree a Nil) = Tree (f a) Nil

mapListOfTrees :: (a -> b) -> List (Tree a) -> List (Tree b)
mapListOfTrees f (Cons (Tree a b) c) = 
    Cons (Tree (f a) (mapListOfTrees f b)) (mapListOfTrees f c)
mapListOfTrees f Nil = Nil

-- data Pair a b = Pair a b

instance Functor (Pair a) where
  fmap f (Pair fst snd) = Pair fst (f snd)

instance Functor (Or a) where
  fmap f (IsA a) = IsA a
  fmap f (IsB b) = IsB (f b)

{-
data ParseResult x =
  ParseError String | ParseSuccess x String

data Parser x = Parser (String -> ParseResult x)
-}

runParser :: Parser a -> String -> ParseResult a
runParser (Parser f) s = f s

instance Functor ParseResult where
  fmap f r = case r of
    ParseError err -> ParseError err
    ParseSuccess a rest -> ParseSuccess (f a) rest

instance Functor Parser where
  fmap :: (a -> b) -> Parser a -> Parser b
  fmap f p = Parser ( \s -> fmap f (runParser p s) )


{- EXERCISE 2: Functor derived operations #-}

(<$>) :: (Functor f) => (a -> b) -> f a -> f b
(<$>) = fmap
infixl 4 <$>

(<$) :: (Functor f) => b -> f a -> f b
b <$ fa = fmap (\a -> b) fa
infixl 4 <$

-- | useful utility function
const :: b -> a -> b
const b = \_ -> b

($>) :: (Functor f) => f a -> b -> f b
fa $> b = fmap (const b) fa
infixl 4 $>

void :: (Functor f) => f a -> f ()
void = fmap (const ())


{- EXERCISE 3: liftF2 -}

{-
addOptional
  :: Optional Int
  -> Optional Int
  -> Optional Int
addOptional optX optY =
  liftF2 (\x y -> x + y) optX optY
  -}

-- Can this function be implemented?  Why / why not?
liftF2
  :: (Functor f)
  => (a -> (b -> c)) -> f a -> f b -> f c
liftF2 abc fa fb = error "todo"


{- EXERCISE 4: liftA2 -}

class (Functor f) => Applicative f where
  (<*>) :: f (a -> b) -> f a -> f b
  pure :: a -> f a
infixl 4 <*>

fmap' :: Applicative f => (a -> b) -> f a -> f b
fmap' f fa =  pure f <*> fa


{- Spot the difference game 

fmap  ::   (a -> b) -> f a -> f b
(<*>) :: f (a -> b) -> f a -> f b

-}

liftA2
  :: (Applicative f)
  => (a -> b -> c) -> f a -> f b -> f c
liftA2 abc fa fb = abc <$> fa <*> fb

-- Observe that the 'f <$> fa <*> fb <*> ...'
-- pattern can be employed for functions of
-- arity 3 (indeed to any arity).
liftA3
  :: (Applicative f)
  => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
liftA3 abcd fa fb fc = abcd <$> fa <*> fb <*> fc

optF :: Optional (Int -> Int)
optF = Full (+1)


{- EXERCIES 5: Applicative instances -}

instance Applicative Optional where
  pure a = Full a
  Full f <*> Full a = Full (f a)
  _ <*> _ = Empty

instance Applicative Vector6 where
  (<*>) = error "todo"
  pure a = Vector6 a a a a a a 

append :: List a -> List a -> List a
append l1 l2 = foldRight Cons l2 l1

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)

instance Applicative List where
  pure a = a `Cons` Nil
  fs <*> fa = case fs of
    Nil -> Nil
    (Cons f fs') -> (fmap f fa) `append` (fs' <*> fa)

instance Applicative NonEmptyList where
  pure a = NonEmptyList a Nil

  NonEmptyList f fs <*> as = case fs of
    Nil -> f <$> as
    (f' `Cons` fs') ->
      (f <$> as) `nelAppend` (NonEmptyList f' fs' <*> as)

nelAppend :: NonEmptyList a -> NonEmptyList a -> NonEmptyList a
nelAppend (NonEmptyList h1 t1) (NonEmptyList h2 t2) =
  NonEmptyList h1 (t1 `append` (h2 `Cons` t2))
    

instance Applicative (Or a) where
  (<*>) = error "todo"
  pure = error "todo"

-- Can this instance be implemented?  Why / why not?
instance Applicative (Pair a) where
  pure = error "todo"
  -- pure a = Pair _ a  -- we are stuck 
  (<*>) = error "todo"

instance Applicative Parser where
  pure a = Parser ( \s -> ParseSuccess a s )
  pf <*> pa = Parser ( \s -> case runParser pf s of
    ParseError err -> ParseError err
    ParseSuccess f rest -> case runParser pa rest of
      ParseError err -> ParseError err
      ParseSuccess a rest2 -> ParseSuccess (f a) rest2
    )


{- EXERCISE 6: Parser functions #-}

-- | Succeeds if string is non-empty and next Char satisfies
--   the predicate
satisfy :: (Char -> Bool) -> Parser Char
satisfy pred = Parser ( \s -> 
  case s of
    "" -> ParseError "end of input"
    (c:rest) | pred c -> ParseSuccess c rest
    _ -> ParseError "satisfy"
  )


-- | Always succeeds if the string is non-empty
anyChar :: Parser Char
anyChar = satisfy (const True)

-- | Parse 't' or 'f', otherwise fail.
-- import Data.Eq ((==))
tOrF :: Parser Char
tOrF = satisfy (\c -> c == 't' || c == 'f')

-- | Parse a digit ('0' .. '9')
digit :: Parser Char
digit = satisfy (\c -> c >= '0' && c <= '9')

-- | Parse a data format that consists of two characters.
--   If the first character is 'd', the next character must be a digit.
--   If the first character is 'b', the next character must be
--     either 't' or 'f'.
--   Return the second character.
parseFormat :: Parser Char
parseFormat = 
  anyChar
    >>= (\c -> case c of
      'd' -> digit
      'b' -> tOrF
      _ -> Parser (\_ -> ParseError "expected 'b' or 'd'")
      )

class Applicative m => Monad m where
  (>>=) :: m a -> (a -> m b) -> m b

instance Monad Parser where
  (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  (>>=) p f = Parser (\s ->
      case runParser p s of
        ParseError err -> ParseError err
        ParseSuccess a rest -> runParser (f a) rest
    )

-- Now you can use (>>=) to implement 'parseFormat'

instance Monad Optional where
  (>>=) = error "todo"

instance Monad List where
  (>>=) = error "todo"

instance Monad (Or a) where
  (>>=) = error "todo"

-- Can you implement this instance?
-- What are the challenges or concerns?
instance Monad Vector6 where
  (>>=) = error "todo"

-- Here, "error" is shadowing "Prelude.error"
ident :: a -> a
ident error = error
