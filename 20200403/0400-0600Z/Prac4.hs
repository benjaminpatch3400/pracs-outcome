{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE NoImplicitPrelude #-}

{-# OPTIONS_GHC -Wall #-}

module Prac4 where

import Data.String
import Data.Eq
import Data.Ord
import Data.Bool
import Data.Char
import Text.Show

import Data.Functor
import Control.Applicative hiding ((<|>), many, some)
import Control.Monad hiding (sequence)


import System.IO
import System.Environment (getArgs)

import Prelude (error, (-), (+), (*), Integer, fromIntegral, seq, IO, (++))

{- TYPES, CLASSES AND FUNCTIONS -}

data Optional a = Empty | Full a
  deriving (Eq, Show)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

data Pair a b = Pair a b
  deriving (Eq, Show)

data NonEmptyList a = NonEmptyList a (List a)
  deriving (Eq, Show)

data ParseResult x =
  ParseError String | ParseSuccess x String
  deriving (Eq, Show)

data Parser x = Parser (String -> ParseResult x)

runParser :: Parser a -> String -> ParseResult a
runParser (Parser f) = f

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)

foldLeft :: (b -> a -> b) -> b -> List a -> b
foldLeft _ b Nil      = b
foldLeft f b (h `Cons` t) = let b' = f b h in b' `seq` foldLeft f b' t

{-
class Functor k where
  fmap :: (a -> b) -> k a -> k b

class (Functor k) => Applicative k where
  (<*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a
infixl 4 <*>

class (Applicative k) => Monad k where
  (>>=) :: k a -> (a -> k b) -> k b
infixl 1 >>=
-}

id :: a -> a
id = \a -> a

const :: b -> a -> b
const b = \_ -> b

{-
(<$>) :: (Functor k) => (a -> b) -> k a -> k b
(<$>) = fmap
infixl 4 <$>

(<$) :: (Functor k) => b -> k a -> k b
b <$ ka = fmap (const b) ka
infixl 4 <$

($>) :: (Functor k) => k a -> b -> k b
ka $> b = fmap (const b) ka
infixl 4 $>

-- | Run two applicative computations, discarding second result.
--   (Note, the <* "points to" the returned value)
(<*) :: (Applicative k) => k a -> k b -> k a
ka <* kb = const <$> ka <*> kb
infixl 4 <*

-- | Run two applicative computations, discarding first result.
--   (Note, the *> "points to" the returned value)
(*>) :: (Applicative k) => k a -> k b -> k b
ka *> kb = (\_ b -> b) <$> ka <*> kb
infixl 4 *>

liftA2 :: (Applicative k) => (a -> b -> c) -> k a -> k b -> k c
liftA2 f ka kb = f <$> ka <*> kb
-}

-- Functor solves problems of shape:
--  liftA1: (a -> b) -> (k a -> k b)   -- aka fmap
--
-- Applicative solves problems of shape:
--  liftA2: (a -> (b -> c)) -> (k a -> (k b -> k c))
--  liftA2: (a ->  b -> c ) ->  k a ->  k b -> k c

{-
instance Functor [] where
  fmap _ [] = []
  fmap f (x:xs) = f x : fmap f xs
-}

{- Isomorphism worked example -}

-- Witness the isomorphism between
--   Optional (NonEmptyList a)
-- and
--   List a
optNonEmptyToList :: Optional (NonEmptyList a) -> List a
optNonEmptyToList opt = case opt of
  Empty -> Nil
  (Full (NonEmptyList x xs)) -> Cons x xs

listToOptNonEmpty :: List a -> Optional (NonEmptyList a)
listToOptNonEmpty l = case l of
  Nil -> Empty
  Cons x xs -> Full (NonEmptyList x xs)

example :: Bool
example =
  let l = Cons 'a' (Cons 'b' (Cons 'c' Nil))
  in
    optNonEmptyToList (listToOptNonEmpty l) == l

{- EXERCISE 1: More Applicative and Monad derived operations -}

-- | "Sequence" means to turn a list of computations into a single
-- computation that collects the results as a list.
sequence :: (Applicative k) => List (k a) -> k (List a)
sequence l = case l of
  Nil -> pure Nil
  Cons x xs -> liftA2 Cons x (sequence xs)

-- | Sequentially execute a list of computations, ignoring results
sequence_ :: (Applicative k) => List (k a) -> k ()
sequence_ xs = sequence xs $> ()

-- NOTE: where values have the same type, you can provide their
-- type signatures in a single declaration, as below for 'when'
-- and 'unless'.  I recommend against this, but you might see it
-- in the wild.

when, when', when'', unless
  :: (Applicative k) => Bool -> k () -> k ()

when b k
  | b = k
  | otherwise = pure ()

when' b k = case b of
  True -> k
  False -> pure ()

when'' True k = k
when'' False _k = pure ()

unless b k = if b then pure () else k

-- | Like (>>=), but reads right-to-left.
--   (Nice to use with /do/ syntax)
--
(=<<) :: (Monad k) => (a -> k b) -> k a -> k b
(=<<) = error "todo"
infixr 1 =<<

join :: (Monad k) => k (k a) -> k a
join = error "todo"


{- EXERCISE 2: Functor, Applicative, Monad for Parser

You might already have done this, but practice is essential
to internalising your understanding of these abstractions,
and how they apply to Parsers.

-}

{-
data ParseResult x =
  ParseError String | ParseSuccess x String
  deriving (Eq, Show)

data Parser x = Parser (String -> ParseResult x)
-}

instance Functor ParseResult where
  fmap f r = case r of
    ParseError err -> ParseError err
    ParseSuccess x rest -> ParseSuccess (f x) rest

instance Functor Parser where
  fmap f p = Parser (
    \s ->
      fmap f (runParser p s)
    )

instance Applicative Parser where
  pure a = Parser (\s -> ParseSuccess a s)

  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  pf <*> pa = Parser
    ( \s ->
      case runParser pf s of
        ParseError err -> ParseError err
        ParseSuccess f rest -> f <$> runParser pa rest
    )

instance Monad Parser where
  (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  pa >>= f = Parser
    ( \s -> case runParser pa s of
      ParseError err -> ParseError err
      ParseSuccess a rest -> runParser (f a) rest
    )


{- EXERCISE 3: Parser building blocks ("combinators") -}

-- | Succeeds if string is non-empty and next Char satisfies
--   the predicate
satisfy :: (Char -> Bool) -> Parser Char
satisfy pred = Parser
  (\s -> case s of
    (c:rest) | pred c -> ParseSuccess c rest
    _ -> ParseError "satisfy"
  )

-- | Parse a specific character
char :: Char -> Parser Char
char c = satisfy (== c)  -- "operation section"
-- char c = satisfy (\c' -> c' == c) -- same thing
-- char c = satisfy (`equal` c) -- same thing (see 'equal')

-- non-infix verison of (==)
equal :: (Eq a) => a -> a -> Bool
equal = (==)

-- | Always succeeds if the string is non-empty
anyChar :: Parser Char
anyChar = satisfy (const True)

-- | Succeed iff all input has been consumed.
endOfInput :: Parser ()
endOfInput = Parser (\s -> case s of
  (_:_) -> ParseError "endOfInput failed"
  _ -> ParseSuccess () ""
  )

-- | If the first parser fails, try the second parser
--
-- Often pronounced "choice", "or", i.e. "this or that"
--
(<|>) :: Parser a -> Parser a -> Parser a
p1 <|> p2 = Parser (\s -> case runParser p1 s of
  success@(ParseSuccess _ _) -> success  -- same as vvv
  -- ParseSuccess a rest -> ParseSuccess a rest   
  ParseError _err -> runParser p2 s
  )

infixl 3 <|>  -- NOTE, lower precendence than <$>, <*>, etc...

-- ...which means this:
--   f <$> p1  <|>  g <$> p2
-- is equivalent to:
--  (f <$> p1) <|> (f <$> p2)
 
-- If the parser succeeds, return @Full r@, otherwise succeed
-- with @Empty@.
optional :: Parser a -> Parser (Optional a)
optional p = Full <$> p <|> pure Empty

-- | Run the parser as many times as possible;
--   collect list of results.
many :: Parser a -> Parser (List a)
many p = liftA2 Cons p (many p) <|> pure Nil

-- | Run the parser as many times as possible;
--   must succeed at least once;
--   collect non-empty list of results.
some :: Parser a -> Parser (NonEmptyList a)
some p = NonEmptyList <$> p <*> many p    -- same as vvv
-- some p = liftA2 NonEmptyList p (many p)

-- Parse zero or more values separated by something
sepBy :: Parser a -> Parser sep -> Parser (List a)
sepBy elem sep =
  liftA2 Cons elem (many (sep *> elem))
  <|> pure Nil


{- EXERCISE 4: NotJSON

NotJSON is a simplified version of JSON.
You will write a parser for it.

- Boolean representation is 't' or 'f'

- Number uses decimal representation (leading '-' for negative)

- String is '"' followed by zero or more characters, ending at
  the next occurrence of '"'.  The double quotes are not part
  of the string.  No escaping facility for '"'.

- Maps are delimited by '{' and '}', key-value pairs are
  separated by ',', keys and value are separated by ':',
  keys are strings.

- No whitespace (except inside strings)

Parser implementations for some of the data types have been
completed for you.

-}

data NJValue
  = NJNumber Integer
  | NJBool Bool
  | NJString String
  | NJMap (List (Pair String NJValue))
  deriving (Eq, Show)

parseNJNumber :: Parser Integer
parseNJNumber = parseSignedInteger

parseNJBool :: Parser Bool
parseNJBool = char 't' $> True <|> char 'f' $> False

parseNJString :: Parser String
parseNJString = fmap listToNativeList
  ( char '"' *> many (satisfy (/= '"')) <* char '"' )

-- type String = [Char]
listToNativeList :: List a -> [a]
listToNativeList = foldRight (:) []

parseNJMap :: Parser (List (Pair String NJValue))
parseNJMap =
  char '{'
  *>
    liftA2 Pair (parseNJString <* char ':') parseNotJSON
    `sepBy` char ','
  <* char '}'

parseNotJSON :: Parser NJValue
parseNotJSON =
  NJNumber <$> parseNJNumber
  <|> NJBool <$> parseNJBool
  <|> NJString <$> parseNJString
  <|> NJMap <$> parseNJMap

-- | Parse a digit ('0' .. '9')
parseDigit :: Parser Integer
parseDigit = c2i <$> satisfy (\c -> c >= '0' && c <= '9')
  where
  c2i c = fromIntegral (ord c - ord '0')

-- | Parse non-signed integer
parseInteger :: Parser Integer
parseInteger = f <$> some parseDigit
  where
  f (NonEmptyList x xs) = foldLeft (\n -> (n * 10 +)) 0 (x `Cons` xs)

parseSignedInteger :: Parser Integer
parseSignedInteger =
  (char '-' $> ((-1) *)) <*> parseInteger
  <|> parseInteger


{- EXERCISE 5: NotJSON pretty printer function

NotJSON is not friendly to human eyes.  Write a pretty printer
program for NotJSON.  This is an open-ended exercise, but here are
some guidelines:

- Nested map structures should be printed with increasing indent

- Maps should be printed with key-value pairs on their own line(s)

- The program should read the name of the file(s) to pretty-print
  from program arguments (or read standard input if args empty).

-}

main :: IO ()
main =
  getArgs >>= \args -> case args of
    (filename:_) -> go filename
    [] -> putStrLn "you didn't specify a file"


-- type FilePath = String
go :: FilePath -> IO ()
go path = readFile path >>= \s ->
  case runParser parseNotJSON s of
    ParseError err -> putStrLn ("parse failed: " ++ err)
    ParseSuccess nj _ ->
      putStrLn "parse succeeded"
      *> pretty nj

pretty :: NJValue -> IO ()
pretty = print -- TODO actually pretty print it
