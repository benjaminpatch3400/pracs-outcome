{- Prac 9: Logic programming with LogicT

Code and exercises in this prac are derived from paper
/Backtracking, Interleaving and Terminating Monad Transformers/
(Kiselyov et al, 2005) [1] and the /logict/ library [2].

[1] http://okmij.org/ftp/papers/LogicT.pdf
[2] https://hackage.haskell.org/package/logict

I recommend reading the paper for a deeper understanding of the
motivation and implementation of these abstractions, and the laws.

-}
{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}

module Prac9 where

import Prelude
  ( ($), (.), const, id, flip, error
  , Int, (+), (-), even, odd, mod
  )

import Data.Bool
import Data.Eq
import Data.Foldable (Foldable(..))
import Data.Functor
import Data.Functor.Identity
import Data.Maybe
import Data.Ord
import Data.Semigroup (Semigroup(..))
import Data.Monoid (Monoid(..))
import Data.Tuple
import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..), (=<<))

{-# ANN module "HLint: ignore Use if" #-}


{- EXERCISE: Church-encoded list -}

data TheOtherList a = Nil | Cons a (TheOtherList a)
-- data [] a = [] | a : ([] a)
-- data [a]  = [] | a :  [a]

newtype List a = List
  { unList :: forall r. (a -> r -> r) -> r -> r }

nil :: List a
nil = List $ \_c n -> n

cons :: a -> List a -> List a
cons h (List t) = List $ \c n -> c h (t c n)

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight f z (List l) = l f z

instance Semigroup (List a) where
  (<>) :: List a -> List a -> List a
  l1 <> l2 = foldRight cons l2 l1 

instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap f (List l) = l (cons . f) nil

instance Applicative List where
  pure :: a -> List a
  pure a = cons a nil

  (<*>) :: List (a -> b) -> List a -> List b
  -- Version 1
  {-
  List fs <*> as =
    List $ \c n ->
      fs (\f r -> (\(List bs) -> bs c r) (fmap f as)) n
  -}

  -- v2
  -- List fs <*> as = fs (\f r -> fmap f as <> r) nil

  -- v3
  fs <*> as = foldRight (\f r -> fmap f as <> r) nil fs


instance Monad List where
  (>>=) :: List a -> (a -> List b) -> List b
  List l >>= f = l ((<>) . f) nil


{- EXERCISE: Alternative and MonadPlus -}

class (Applicative k) => Alternative k where
  -- | failure
  empty :: k a

  -- | choice
  (<|>) :: k a -> k a -> k a

guard :: (Alternative k) => Bool -> k ()
guard b = case b of
  True -> pure ()
  False -> empty

-- | Like Alternative, with additional laws related to Monad.
--
class (Alternative k, Monad k) => MonadPlus k where
  -- mzero >>= f  =  mzero
  -- a *> mzero   =  mzero
  mzero :: k a
  mzero = empty

  -- distributive law:
  -- (a `mplus` b) >>= f  =  (a >>= f) `mplus` (b >>= f)
  mplus :: k a -> k a -> k a
  mplus = (<|>)


instance Alternative List where
  empty = nil
  (<|>) = (<>)

instance MonadPlus List where
  -- use default implementations


{- EXERCISE: LogicT -}

-- | This is the Church-encoded list transformer.  We will equip
--   it with special behaviour for logic programming later.
--
newtype LogicT k a = LogicT
  { unLogicT :: forall r. (a -> k r -> k r) -> k r -> k r }
{- COMPARE (definiion from above)
newtype List a = List
  { unList   :: forall r. (a ->   r ->   r) ->   r ->   r }
-}

-- recover 'List' from LogicT
type List' = LogicT Identity

instance Functor (LogicT k) where
  fmap :: (a -> b) -> LogicT k a -> LogicT k b
  fmap f (LogicT l) =
    LogicT $ \c n -> l (c . f) n

nil' :: LogicT k a
nil' = LogicT $ \_c n -> n

instance Applicative (LogicT k) where
  pure :: a -> LogicT k a
  pure a = LogicT $ \c n -> c a n

  (<*>) :: LogicT k (a -> b) -> LogicT k a -> LogicT k b
  {-
  LogicT fs <*> as =
    LogicT $ \c n ->
      fs (\f r -> unLogicT (fmap f as) c r) n
  -}
  LogicT fs <*> LogicT as =
    LogicT $ \c n ->
      fs (\f r -> as (c . f) r) n

instance Monad (LogicT k) where
  (>>=) :: LogicT k a -> (a -> LogicT k b) -> LogicT k b
  LogicT as >>= f =
    LogicT $ \c n ->
      as (\a r -> unLogicT (f a) c r) n

instance Alternative (LogicT k) where
  empty :: LogicT k a
  empty = nil'

  (<|>) :: LogicT k a -> LogicT k a -> LogicT k a
  LogicT l1 <|> LogicT l2 =
    LogicT $ \c n -> l1 c (l2 c n)

instance MonadPlus (LogicT k) where
  -- use default implementations



{- EXERCISE: Foldable LogicT -}

instance (Applicative k, Foldable k) => Foldable (LogicT k) where
  foldMap :: (Monoid m) => (a -> m) -> LogicT k a -> m
  foldMap f (LogicT l) =
    fold $ l (\a r -> fmap (f a <>) r) (pure mempty)

asum :: (Foldable t, Alternative k) => t (k a) -> k a
asum = foldr (<|>) empty


{- EXERCISE: LogicT combinators -}

lift :: (Monad k) => k a -> LogicT k a
lift k = LogicT $ \c n -> k >>= \a -> c a n

reflect :: (Alternative k) => Maybe (a, k a) -> k a
reflect mk = case mk of
  Nothing -> empty
  Just (a, k) -> pure a <|> k

msplit
  :: (Monad k)
  => LogicT k a -> LogicT k (Maybe (a, LogicT k a))
msplit (LogicT l) =
  lift $
    l
      (\a r -> pure (Just (a, lift r >>= reflect)))
      (pure Nothing)

-- | Fair disjunction
interleave
  :: (Monad k)
  => LogicT k a -> LogicT k a -> LogicT k a
interleave l r =
  msplit l
  >>= maybe r (\(a, l') -> pure a `mplus` interleave r l')

-- | Fair conjunction
--
-- (a `mplus` b) >>= k  ~  a >>= k `mplus` b >>= k
--
-- If (a >>= k) does not terminate then the (b >>= k)
-- branch will never be considered.  So we need a
-- "fair" variation of (>>=)
--
(>>-) :: (Monad k) => LogicT k a -> (a -> LogicT k b) -> LogicT k b
k >>- f =
  msplit k
  >>= maybe mzero
        (\(a, k') -> interleave (f a) (k' >>- f))

-- | Logical conditional
--
-- if t_hen e_lse
ifte
  :: (Monad k)
  => LogicT k a         -- ^ test
  -> (a -> LogicT k b)  -- ^ success branch
  -> LogicT k b         -- ^ failure branch
  -> LogicT k b
ifte l t f =
  msplit l
  >>= maybe f (\(a, l') -> t a `mplus` (l' >>= t))

-- | Pruning; select one result
once :: (Monad k) => LogicT k a -> LogicT k a
once l = msplit l >>= maybe mzero (pure . fst)

-- | Logical negation
lnot :: (Monad k) => LogicT k a -> LogicT k ()
lnot l = ifte (once l) (const mzero) (pure ())


{- EXERCISE: observing results -}

-- | Extract first result
observeT :: (Applicative k) => LogicT k a -> k (Maybe a)
observeT (LogicT l) =
  l (\a _r -> pure (Just a)) (pure Nothing)

-- | Extract all results
observeAllT :: (Applicative k) => LogicT k a -> k [a]
observeAllT (LogicT l) = l (fmap . (:)) (pure [])

-- | Extract up to /n/ results
observeManyT :: (Monad k) => Int -> LogicT k a -> k [a]
observeManyT n k
  | n <= 0 = pure []
  | n == 1 = maybe [] pure <$> observeT k
  | otherwise =
      unLogicT (msplit k)
        (\ma r ->
          maybe
            r
            (\(a, k') -> (a:) <$> observeManyT (n - 1) k')
            ma
        )
        (pure [])


{- EXERCISE: Logic -}

type Logic = LogicT Identity

observe :: Logic a -> Maybe a
observe = runIdentity . observeT

observeAll :: Logic a -> [a]
observeAll = runIdentity . observeAllT

observeMany :: Int -> Logic a -> [a]
observeMany n = runIdentity . observeManyT n


{- EXERCISE: building logic computations -}

odds :: LogicT k Int
odds = pure 1 `mplus` fmap (+2) odds

oddsPlus :: Int -> LogicT k Int
oddsPlus n = fmap (+n) odds

zeroAndOne :: LogicT k Int
zeroAndOne = pure 0 `mplus` pure 1

oddsAndEvensNaïve :: LogicT k Int
oddsAndEvensNaïve =
  zeroAndOne >>= oddsPlus

oddsAndEvensFair :: (Monad k) => LogicT k Int
oddsAndEvensFair =
  zeroAndOne >>- oddsPlus

-- | Return even numbers from the input
--
-- What happens if you apply to oddsAndEvensFair?
-- What happens if you apply to oddsAndEvensNaïve?
--
evensFrom :: LogicT k Int -> LogicT k Int
evensFrom k = do
  n <- k
  if even n then pure n else mzero

-- | Generate ints from 1..n
iota :: (Alternative k) => Int -> k Int
iota n = asum (pure <$> [1..n])

oddPrimes :: (Monad k) => LogicT k Int
oddPrimes = do
  n <- odds
  guard (n > 1)
  lnot $ do
    d <- iota (n - 1)
    guard (d > 1)
    guard (n `mod` d == 0)
  pure n

-- use 'oddPrimes' and @pure 2@
primes :: (Monad k) => LogicT k Int
primes = oddPrimes `interleave` pure 2

-- use 'primes'
evenPrimes :: (Monad k) => LogicT k Int
evenPrimes = do
  n <- primes
  guard (even n)
  pure n

