{- Prac 9: Logic programming with LogicT

Code and exercises in this prac are derived from paper
/Backtracking, Interleaving and Terminating Monad Transformers/
(Kiselyov et al, 2005) [1] and the /logict/ library [2].

[1] http://okmij.org/ftp/papers/LogicT.pdf
[2] https://hackage.haskell.org/package/logict

I recommend reading the paper for a deeper understanding of the
motivation and implementation of these abstractions, and the laws.

-}
{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}

module Prac9 where

import Prelude
  ( ($), (.), const, id, flip, error
  , Int, (+), (-), even, odd, mod
  )


import Data.Bool
import Data.Eq
import Data.Foldable (Foldable(..))
import Data.Functor
import Data.Functor.Identity
import Data.Maybe
import Data.Ord
import Data.Semigroup (Semigroup(..))
import Data.Monoid (Monoid(..))
import Data.Tuple
import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..), (=<<))


{- EXERCISE: Church-encoded list -}

-- The list we are use to:
--   data List a = Nil | Cons a (List a)

-- A different way to define lists
--
newtype List a = List
  { unList :: forall r. (a -> r -> r) -> r -> r }

-- Church (or Scott) encoding can be used to define any
-- data type.  For example here is Bool
--
newtype ChurchBool = ChurchBool
  { runBool :: forall r. r -> r -> r }

true, false :: ChurchBool
true = ChurchBool (\_ r -> r)
false = ChurchBool const

-- Good blog post about church and scott encoding:
-- https://bradparker.com/content/posts/2020-01-27-getting-close-to-the-conceptual-metal.html

nil :: List a
nil = List $ \_c n -> n

cons :: a -> List a -> List a
cons h (List t) = List $ \c n -> c h (t c n)

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight f z (List l) = l f z

instance Semigroup (List a) where
  (<>) :: List a -> List a -> List a
  l1 <> l2 = foldRight cons l2 l1

instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap f = foldRight (cons . f) nil

instance Applicative List where
  pure :: a -> List a
  pure a = cons a nil

  (<*>) :: List (a -> b) -> List a -> List b
  List fs <*> as = fs (\f r -> fmap f as <> r) nil

instance Monad List where
  (>>=) :: List a -> (a -> List b) -> List b
  List as >>= f = as (\a r -> f a <> r) nil


{- EXERCISE: Alternative and MonadPlus -}

class (Applicative k) => Alternative k where
  -- | failure
  empty :: k a

  -- | choice
  (<|>) :: k a -> k a -> k a

guard :: (Alternative k) => Bool -> k ()
guard True = pure ()
guard False = empty

-- | Like Alternative, with additional laws related to Monad.
--
class (Alternative k, Monad k) => MonadPlus k where
  -- mzero >>= k       ~   mzero
  -- k >>= const mzero ~   mzero
  mzero :: k a
  mzero = empty

  -- (a `mplus` b) >>= f  ~  (a >>= f) `mplus` (b >>= f)
  mplus :: k a -> k a -> k a
  mplus = (<|>)


instance Alternative List where
  empty = nil
  (<|>) = (<>)

instance MonadPlus List where
  -- use default implementations


{- EXERCISE: LogicT -}

-- | This is the Church-encoded list transformer.  We will equip
--   it with special behaviour for logic programming later.
--
newtype LogicT k a = LogicT
  { unLogicT :: forall r. (a -> k r -> k r) -> k r -> k r }
{-
newtype List a = List
  { unList :: forall r.   (a ->   r ->   r) ->   r ->   r }
-}

instance Functor (LogicT k) where
  fmap :: (a -> b) -> LogicT k a -> LogicT k b
  fmap f (LogicT l) =
    LogicT $ \c n -> l (c . f) n

instance Applicative (LogicT k) where
  pure :: a -> LogicT k a
  pure a = LogicT $ \c n -> c a n

  (<*>) :: LogicT k (a -> b) -> LogicT k a -> LogicT k b
  LogicT fs <*> LogicT as =
    LogicT $ \c n -> fs (\f r -> as (c . f) r) n

instance Monad (LogicT k) where
  (>>=) :: LogicT k a -> (a -> LogicT k b) -> LogicT k b
  LogicT as >>= f =
    LogicT $ \c n ->
      as (\a r -> unLogicT (f a) c r) n

instance Alternative (LogicT k) where
  empty :: LogicT k a
  empty = LogicT $ \_c n -> n

  (<|>) :: LogicT k a -> LogicT k a -> LogicT k a
  LogicT l1 <|> LogicT l2 =
    LogicT $ \c n -> l1 c (l2 c n)
      

instance MonadPlus (LogicT k) where
  -- use default implementations

{- EXERCISE: Foldable (LogicT k) -}

instance (Applicative k, Foldable k) => Foldable (LogicT k) where
  foldMap :: (Monoid m) => (a -> m) -> LogicT k a -> m
  foldMap f (LogicT l) =
    fold $ l (\a km -> fmap (f a <>) km) (pure mempty)

asum :: (Foldable t, Alternative k) => t (k a) -> k a
asum = foldr (<|>) empty


{- EXERCISE: LogicT combinators -}

lift :: (Monad k) => k a -> LogicT k a
lift k = LogicT $ \c n -> k >>= \a -> c a n

reflect :: (Alternative k) => Maybe (a, k a) -> k a
reflect m = case m of
  Nothing -> empty
  Just (a, k) -> pure a <|> k

msplit
  :: (Monad k)
  => LogicT k a -> LogicT k (Maybe (a, LogicT k a))
msplit (LogicT l) =
  lift $
    l (\a r -> pure (Just (a, lift r >>= reflect))) (pure Nothing)

-- | Fair disjunction
interleave :: (Monad k) => LogicT k a -> LogicT k a -> LogicT k a
interleave l r =
  msplit l
  >>= maybe
        r
        (\(a, l') -> pure a `mplus` interleave r l')

-- | Fair conjunction
(>>-) :: (Monad k) => LogicT k a -> (a -> LogicT k b) -> LogicT k b
l >>- f =
  msplit l
  >>= maybe
        mzero
        (\(a, l') -> interleave (f a) (l' >>- f))

-- | Logical conditional
ifte
  :: (Monad k)
  => LogicT k a         -- ^ test
  -> (a -> LogicT k b)  -- ^ success branch
  -> LogicT k b         -- ^ failure branch
  -> LogicT k b
ifte l t f =
  msplit l
  >>= maybe f (\(a, l') -> t a `mplus` (l' >>= t))

-- | Pruning; select one result
once :: (Monad k) => LogicT k a -> LogicT k a
once l = msplit l >>= maybe mzero (pure . fst)

-- | Logical negation
lnot :: (Monad k) => LogicT k a -> LogicT k ()
lnot l = ifte (once l) (const mzero) (pure ())



{- EXERCISE: observing results -}

-- | Extract first result
observeT :: (Applicative k) => LogicT k a -> k (Maybe a)
observeT (LogicT l) = l (\a _r -> pure (Just a)) (pure Nothing)

-- | Extract all results
observeAllT :: (Applicative k) => LogicT k a -> k [a]
observeAllT (LogicT l) = l (\a -> fmap (a:)) (pure [])

-- | Extract up to /n/ results
observeManyT :: (Monad k) => Int -> LogicT k a -> k [a]
observeManyT n k
  | n <= 0  = pure []
  | n == 1  = maybe [] pure <$> observeT k
  | otherwise =
      unLogicT (msplit k)
        (\ma r ->
          maybe
            r
            (\(a, k') -> (a:) <$> observeManyT (n - 1) k')
            ma
        )
        (pure [])


{- EXERCISE: Logic -}

type Logic = LogicT Identity

observe :: Logic a -> Maybe a
observe = runIdentity . observeT

observeAll :: Logic a -> [a]
observeAll = runIdentity . observeAllT

observeMany :: Int -> Logic a -> [a]
observeMany n = runIdentity . observeManyT n


{- EXERCISE: building logic computations -}

odds :: LogicT k Int
odds = error "todo"

oddsPlus :: Int -> LogicT k Int
oddsPlus = error "todo"

zeroAndOne :: LogicT k Int
zeroAndOne = error "todo"

oddsAndEvensNaïve :: LogicT k Int
oddsAndEvensNaïve = error "todo"

oddsAndEvensFair :: (Monad k) => LogicT k Int
oddsAndEvensFair = error "todo"

-- | Return even numbers from the input
--
-- What happens if you apply to oddsAndEvensFair?
-- What happens if you apply to oddsAndEvensNaïve?
--
evensFrom :: LogicT k Int -> LogicT k Int
evensFrom = error "todo"

-- | Generate ints from 1..n
iota :: (Alternative k) => Int -> k Int
iota = error "todo"

oddPrimes :: (Monad k) => LogicT k Int
oddPrimes = error "todo"

-- use 'oddPrimes' and @pure 2@
primes :: (Monad k) => LogicT k Int
primes = error "todo"

-- use 'primes'
evenPrimes :: (Monad k) => LogicT k Int
evenPrimes = error "todo"
