module Prac10 where

import Control.Applicative
import Data.Foldable hiding (asum)

{- Where do you want to go today? -}


{-

Question: when I do 'asum (pure <$> l)' what is happening?

-}

asum :: (Foldable t, Alternative k) => t (k a) -> k a
asum = foldr (<|>) empty

iota :: (Alternative k) => Int -> k Int
iota n = asum (pure <$> [1..n])

iota5 :: (Alternative k) => k Int
iota5 = iota 3
{-

Let's substitute definitions to see what this works out to:

@
      = asum (pure <$> [1..3])
        -- subst fmap def
      = asum [pure 1, pure 2, pure 3]
        -- subst asum def
      = foldr (<|>) empty [pure 1, pure 2, pure 3]
        -- desugar list syntax
      = foldr (<|>) empty ( pure 1 : pure 2 : pure 3 : [] )
        -- foldr does constructor replacement
      = pure 1 <|> (pure 2 <|> (pure 3 <|> empty))
        -- ^ note, <|> is infixl so we need parens above;
        --   it computes the same result 
@
      
Note; the result depends on the instantiation of
'Applicative k => k'.  if 'k ~ []', then asum appends lists.

-}


-- What else could we use asum to do?  We have a choice of
-- (Applicative k => k) and (Foldable t => t).
-- The choice of 't' is not very interesting; it is just a
-- container.

-- | Get the first non-Nothing value from the sequence
--
-- > firstJust [Nothing, Nothing, Nothing]
-- Nothing
-- > firstJust [Nothing, Just 'a', Just 'b', Nothing]
-- Just 'a'
--
firstJust :: (Foldable t) => t (Maybe a) -> Maybe a
firstJust = asum

{-

What else has an Alternative instance?

:info Alternative

We see that IO has an alternative instance.  I wonder what it does.
Let's find out.  The below transcript is from our experimentation
during prac:

λ> getChar <|> getChar
a'a'

So under what circumstances would the second branch execute?  I had
to read the source code from /base/ library.  We learned that the
second computation executes if the first threw an exception.

Maybe we can use instance Alternative IO to read config files with
fallback?  Let's give it a go.

-}

-- | List of config files in order of priority.
-- Some of these are just made up because I can't remember
-- where they actually live :)
--
configFiles :: [FilePath]  -- type FilePath = String
configFiles =
  [ ".ghci"  -- current dir (there is no such file in this dir)
  , "/home/ftweedal/.ghci" -- user config (this does exist)
  , "/usr/share/ghc/config/.ghci" -- global config (wild guess)
  ]

readConfig :: IO String
readConfig = asum (readFile <$> configFiles)

{-

Observe: readConfig "fell through" to the second config file,
gobbling up the exception that @readFile ".ghci"@ throws.

λ> readConfig
":set prompt \"\955> \"\n:set -XOverloadedStrings\n:set -XFlexibleContexts\n"
λ> readFile ".ghci"
*** Exception: .ghci: openFile: does not exist (No such file or directory)

-}



{- 

Question: in practice, do you always write total functions?


Me (Fraser) personally, yes, with limited exceptions.  Even if I
"know" a case cannot occur, I will still handle it.  It should be a
priority to write total functions, because you do not know how the
system may change in the future.  The presence of non-total
functions today, even if they are "being used in a total way" e.g.
with guards against the "bad" inputs, a small change in the future
could lead to catastrophe.

There are some exception though, e.g. division by zero.  You can't
really avoid arithmetic.  Haskell's arithmetic functions are
non-total, so you have to live with that, and have whatever
checks/guards are necessary to prevent errant behaviour.

Friends don't let friends use 'head'.  'head', 'tail', 'init',
'last', 'read', (!!), etc exist.  You should not use them.  Unlike
`div`, these are never needed.  Total solutions are better.

Also, IO can fail and in Haskell exceptions are used.  The APIs
could be better, but it is not hard to write safer APIs.  e.g. I
just used 'readFile' above, in an unsafe way.  Witness:

λ> asum (readFile <$> [])
*** Exception: user error (mzero)

Let's write a better variant of 'readConfig':

-}

-- | Try and read the files in the list, returning the
-- data from the first successfully read file.  If none
-- of the files can be read (e.g. don't exist or don't have
-- permissions) then return an empty config.
--
readConfigBetter :: IO String
readConfigBetter = asum $
  (readFile <$> configFiles)
  <> [pure "" {- empty config -}]

{-

In summary: I do sometimes have to /use/ some non-total functions,
but the functions I write are almost always total.

Many functional programmers have the same disposition.  I don't know
any who don't care about writing total functions.

-}


{-

Question: how hard is it to get into real production code vs
where we are now?

Thoughts in brief: it is not hard.  You just need to work out
what itch you want to scratch.  Fix a bug, find a project/tool
that interested you and find something to do.

Thoughts:

* when I was researching LogicT for last week's prac, I noticed
  that some functions in the /logict/ library have redundant or
  overtight constraints.  Easy fix, get some code in a real
  library.

* let us (me, Tony, Brian) know what interests you; maybe we
  know about a library or project in that space (or we know
  who/where to ask to find out).

* Where to next if you want to keep doing FP and learn more?
  Brisbane Functional Programming Group (BFPG); meets monthly
  (modulo COVID) + sometimes hack nights; #bfpg on Freenode.

-}


{-

Question: Getting the 'a' out of a 'k a'

-}

getTheA :: (Applicative k) => k a -> a
getTheA = error "can we do it?"

getMaybe :: Maybe a -> a
getMaybe (Just a) = a
getMaybe Nothing = error "welp"
-- ^ it compiles, but that is not a solution (non-total).
-- We can 'fromJust' to the list "functions to never use" above

-- the reasoning: if there is a constructor that does not
-- "contain" an 'a', we cannot implement a function 'k a -> a'
-- So this will be the case for List / [] also.

-- one constructor, and it "contains" at least one 'a'
data NonEmptyList a = a :| [a]

-- we can /can/ write this one:
getNonEmpty :: NonEmptyList a -> a
getNonEmpty (a :| _) = a

-- rose trees (there is always an 'a')
data Tree a = Tree a [Tree a]

-- so we can do this too:
getTree :: Tree a -> a
getTree (Tree a _) = a

-- can we do it for IO?
--
-- we know IO a /ostensibly/ will produce an a /when executed/
-- But we can't just execute IO anywhere - we can only do that
-- in 'main :: IO ()' or by (>>=) (which means we "stay in IO").
--
-- So we cannot "get the a out of an IO a"
--
getIO :: IO a -> a
getIO = error "nope, can't do it"

-- Comment: "I have no clue how IO works under the hood"
-- This is totally fine.  You don't need to know, and it is
-- better not to know.  Knowing how IO works under the hood is
-- **advanced Haskell**.
--
-- Note: there /is/ actually a (dangerous) way to do it.
-- I am not going to tell you what that is.  It, also, is
-- advanced Haskell, and should be avoided unless you really
-- know what you're doing.  There be dragons.


-- What about... (exercises for student)

newtype Konst r a = Konst r

getKonst :: Konst r a -> a
getKonst = error "todo"


newtype Tagged t a = Tagged a

getTagged :: Tagged t a -> a
getTagged = error "todo"


newtype Identity a = Identity a

getIdentity :: Identity a -> a
getIdentity = error "todo"



{-

Question: How often do I think about efficiency?

Answer: there are two parts

  a) Don't do silly things (e.g. don't be quadratic).
     In Haskell, pay attention to associativity of (infix)
     functions, e.g. (<>) is infixr, so 'foldr (<>) mempty'
     is O(n^2) if (<>) is O(n).  This applies for any language.

  b) Modulo (a), I only think about performance when I
     notice it is bad.

A really good point: "apparently Church-encoded lists can (<>)
in constant time".  This is true.  Sometimes a different
(isomorphic) representation of a datum can have a profound
impact on performance.

Case study: /bytestring/ and /text/ library.  There are multiple
representations in these libraries, with differing asymptotics,
and functions to convert between them.

In non-strict languages like Haskell there's another potential
for performance issues in "space leaks", i.e. accumulation of
"thunks" (unevaluated functions).  This can be observed in
foldl and is why we have foldl' (strict in accumulator).

λ> :t foldl
foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b
λ> :t foldl'
foldl' :: Foldable t => (b -> a -> b) -> b -> t a -> b

(tl;dr: always use foldl')

You may see this "pattern" - i.e. variant of some function
that is "strict in some argument" - in other places/libraries.


Some benchmarking/profiling tools:

- GHC can build profiling support into libraries and binaries;
  see user guide for details.

- Criterion is a best-in-class benchmarking library for Haskell

- /threadscope/ tool

-}
